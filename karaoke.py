#!/usr/bin/python3
# -*- coding: utf-8 -*-

from xml.sax import make_parser
from xml.sax.handler import ContentHandler
from smil import SMILHandler
import json
import sys


  
def to_string(lista): #Función que devuelve etiqueta, atributos y contenido de tal forma: Elemento1\tAtributo11="Valor11"\tAtributo12="Valor12"\t...\n (tabuladores y saltos de línea)  

    final = ""
    for elemento in lista:
        nombre = elemento[0]
        etiqueta = elemento[1]
        final += (f"\n{nombre}") 
        for atributo in etiqueta:
            valor = etiqueta[atributo]
            if valor != "":
                final += (f"\t{atributo} = {valor}\t")
    return final



def to_json(lista, fichero): #Función que crea un fichero .json a partir de un fichero .smil 
    ficherojson = ""
    if not ficherojson:
        ficherojson = fichero.replace(".smil", ".json")
        with open(ficherojson, "w") as outfile:
            json.dump(lista, outfile, indent=1)


    length = 0 
    final = "[\n" + "\t" + "{"
    for elemento in lista:
        nombre = elemento[0]
        etiqueta = elemento[1]
        final += '"name": "' + nombre + '",\n' + '\t "attrs": {\n'
        for atributo in etiqueta:
            valor = etiqueta[atributo]
            final += '\t  "' + atributo + '": "' + valor + '",\n'
        length = length + 1
        if length == (len(lista)):
            final += '\t  }\n' + '\t}\n'
        else:
            final += '\t  }\n' + '\t},\n' + '\t{' + ']'
    return final #Formato en el que devuelve el contenido del fichero .json



def main():
    global fichero
    global ficherojson
    parser = make_parser()
    cHandler = SMILHandler()
    parser.setContentHandler(cHandler)
    parser.parse(open(fichero))
    lista = cHandler.get_tags()

    
    print(to_string(lista)) #Imprime por pantalla los métodos definidos anteriormente
    print(to_json(lista, fichero))



if __name__ == "__main__":

    try:
        fichero = sys.argv[1] 


    except IndexError:
        sys.exit('Usage: python3 karaoke.py <file>') #Si no se especifica una extensión del fichero imprime el mensaje de error
    

    main()


   






    

  




