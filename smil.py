#!/usr/bin/python3
# -*- coding: utf-8 -*-

from xml.sax import make_parser
from xml.sax.handler import ContentHandler



class SMILHandler(ContentHandler): #clase que hereda de la clase "ContentHandler"

    def __init__(self): #Inicialización de variables

        self.diccionario = {} #Diccionario vacío en el que posteriormente se almacenan los atributos y su contenido
        self.lista = [] #Lista vacía en el que se posteriormente se almacenan cada una de las etiquetas con sus correspondientes atributos y su contenido


    def startElement(self, name, attrs): #Función que se llama cuando comienza una etiqueta 

        if name == 'root-layout': #Nombre etiqueta
            self.diccionario['width'] = attrs.get('width', "")
            self.diccionario['height'] = attrs.get('height', "")
            self.diccionario['background-color'] = attrs.get('background-color', "")
            self.lista.append([name, self.diccionario])
            self.diccionario = {}

        elif name == 'region': #Nombre etiqueta
            self.diccionario['id'] = attrs.get('id', "") #Añade al diccionario creado cada uno de los atributos correspondientes
            self.diccionario['top'] = attrs.get('top', "")
            self.diccionario['bottom'] = attrs.get('bottom', "")
            self.diccionario['left'] = attrs.get('left', "")
            self.diccionario['right'] = attrs.get('right', "")
            self.lista.append([name, self.diccionario]) #Añade a la lista creada el nombre de la etiqueta y lo añadido al diccionario (atributos y contenido de estos)
            self.diccionario = {}

        elif name == 'img': #Nombre etiqueta
            self.diccionario['src'] = attrs.get('src', "")
            self.diccionario['region'] = attrs.get('region', "")
            self.diccionario['begin'] = attrs.get('begin', "")
            self.diccionario['dur'] = attrs.get('dur', "")
            self.lista.append([name, self.diccionario])
            self.diccionario = {}

        elif name == 'audio': #Nombre etiqueta
            self.diccionario['src'] = attrs.get('src', "")
            self.diccionario['begin'] = attrs.get('begin', "")
            self.diccionario['dur'] = attrs.get('dur', "")
            self.lista.append([name, self.diccionario])
            self.diccionario = {}

        elif name == 'textstream': #Nombre etiqueta
            self.diccionario['src'] = attrs.get('src', "")
            self.diccionario['region'] = attrs.get('region', "")
            self.lista.append([name, self.diccionario])
            self.diccionario = {}

    def get_tags(self): #Función que devuelve la lista con etiquetas, sus correspondientes atributos y el contenido
        return self.lista



def main():
    parser = make_parser()
    cHandler = SMILHandler()
    parser.setContentHandler(cHandler)
    parser.parse(open('karaoke.smil')) #Ejemplo con el fichero "karaoke.smil"
    print(cHandler.get_tags())


if __name__ == "__main__":
    main()




        
