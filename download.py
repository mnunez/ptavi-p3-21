from xml.sax import make_parser
from smil import SMILHandler
from xml.sax.handler import ContentHandler
import sys
import urllib.request


def download(lista): #Función que descarga en local el contenido multimedia de un archivo con extensión .smil

    for elemento in lista:
        etiqueta = elemento[1]
        for atributo in etiqueta:
            valor = etiqueta[atributo]
            if atributo == 'src':
                if valor.startswith("http://" or "https://"):
                    url = valor 
                    file = url.split('/')[-1]
                    urllib.request.urlretrieve(url, file)


def main():
    global file
    parser = make_parser()
    cHandler = SMILHandler()
    parser.setContentHandler(cHandler)
    parser.parse(open(file))
    lista = cHandler.get_tags()

    print(download(lista))


if __name__ == "__main__":
    try:
        file = sys.argv[1]
    except IndexError:
        sys.exit('Usage: python3 karaoke.py <file>')

    main()

    

