#!/usr/bin/python3
# -*- coding: utf-8 -*-

from xml.sax import make_parser
from xml.sax.handler import ContentHandler


class ChistesHandler(ContentHandler):
    """
    Clase para manejar chistes malos
    """

    def __init__(self):
        """
        Constructor. Inicializamos las variables
        """
        self.num = 0
        self.calificacion = ""
        self.pregunta = ""
        self.inPregunta = False
        self.respuesta = ""
        self.inRespuesta = False
        self.nombre = ""

    def startElement(self, name, attrs):
        """
        Método que se llama cuando se abre una etiqueta
        """
        if name == 'chiste':
            # De esta manera tomamos los valores de los atributos
            self.calificacion = attrs.get('calificacion', "")
            self.num = self.num + 1
            print(f"Chiste número: {self.num} ({self.calificacion})")
            self.nombre = name
        elif name == 'pregunta':
            self.pregunta = ""
            self.inPregunta = True
            self.nombre = name
        elif name == 'respuesta':
            self.respuesta = ""
            self.inRespuesta = True
            self.nombre = name

    def endElement(self, name):
        """
        Método que se llama al cerrar una etiqueta
        """
        if name == 'chiste':
            self.pregunta = ""
            self.respuesta = ""
        elif name == 'pregunta':
            self.inPregunta = False
        elif name == 'respuesta':
            self.inRespuesta = False

    def characters(self, char):
        """
        Método para tomar contenido de la etiqueta
        """
        if self.inPregunta:
            self.pregunta = self.pregunta + char
        elif self.inRespuesta:
            self.respuesta += char


        if self.pregunta != "":
            if self.respuesta != "":
                if char == "\n":
                    if self.nombre == 'pregunta' or self.nombre == 'respuesta':
                        print(f"Pregunta: {self.pregunta}")
                        print(f"Respuesta: {self.respuesta}")
                        print() 


def main():
    """Programa principal"""
    parser = make_parser()
    cHandler = ChistesHandler()
    parser.setContentHandler(cHandler)
    parser.parse(open('chistes.xml'))


if __name__ == "__main__":
    main()
