#!/usr/bin/python3
# -*- coding: utf-8 -*-

from xml.sax import make_parser
from xml.sax.handler import ContentHandler
from smil import SMILHandler
import urllib.request
import json
import sys


class Karaoke():

    def __init__(self, fichero): #Inicializador
        parser = make_parser()
        cHandler = SMILHandler()
        parser.setContentHandler(cHandler)
        parser.parse(open(fichero))
        self.tags = cHandler.get_tags() #variable de la instancia tags


    def to_string(self):   
        final = ""
        for elemento in self.tags:
            nombre = elemento[0]
            etiqueta = elemento[1]
            final += (f"\n{nombre}") 
            for atributo in etiqueta:
                valor = etiqueta[atributo]
                if valor != "":
                    final += (f"\t{atributo} = {valor}\t")
        return final


    def to_json(self, fichero):
        ficherojson = ""
        if not ficherojson:
            ficherojson = fichero.replace(".smil", ".json")
            with open(ficherojson, "w") as outfile:
                json.dump(self.tags, outfile, indent=1)


    def download(self): 
        for elemento in self.tags:
            etiqueta = elemento[1]
            for atributo in etiqueta:
                valor = etiqueta[atributo]
                if atributo == 'src':
                    if valor.startswith("http://" or "https://"):
                        url = valor 
                        file = url.split('/')[-1]
                        urllib.request.urlretrieve(url, file)




if __name__ == "__main__":

    try:
        fichero = sys.argv[1] 

    except IndexError:
        sys.exit('Usage: python3 karaobjeto.py [--json] <file>')

    karaoke = Karaoke(fichero)
    

    if fichero:
        print(karaoke.to_string())
    
    else:
        print(karaoke.to_json(fichero))

    print(karaoke.download())
       
    

        

    
